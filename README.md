*The modern alternative to QuickSFV using Rust and using [crc32fast](https://crates.io/crates/crc32fast) crate.*

## What the hell is that?!
This is a dashHash. The program to validate integrity of files using the CRC32 checksum. It can be used to check a list of files against a .sfv file list, and to generate new lists.
Originally, was made as alternative for QuickSFV.

## Features
Currently, able to check files and making hashes.

## Where I can get it?!

Click on [releases](https://codeberg.org/CerdaCodes/DashHash/releases) and get an .exe if you are on Windows, or without file extension is Linux. 
Also, don't forget to grab batch scripts!

## How to build?!
Get Rust, run in terminal or in cmd
cargo build --release

## License
[GNU GPL v3 only](https://choosealicense.com/licenses/gpl-3.0/).
